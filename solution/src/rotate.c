
#include "rotate.h"

#include <stddef.h>

static struct pixel pixel_at( struct pixel* pixels, const size_t x, const size_t y, const uint64_t width ) 
{
	return pixels[y * width + x];
}

struct image rotate( struct image const source )
{
	if ( source.data == NULL )
	{
		return (struct image) {0};
	}

	struct image rotated_img = image_create(source.height, source.width);
	
	if ( rotated_img.data == NULL )
	{
		return (struct image) {0};
	}
	
	for ( size_t i = 0; i < source.width; i++ ) 
	{
		for ( size_t j = 0; j < source.height; j++ ) 
		{
			rotated_img.data[i * rotated_img.width + rotated_img.width - j - 1] = pixel_at(source.data, i, j, source.width);
		}
	}
	
	return rotated_img;
}
