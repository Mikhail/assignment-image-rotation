
#include "bmp.h"

#include <stdint.h>
#include <stdlib.h>

#define BF_TYPE 19778
#define BI_SIZE 40
#define BI_BIT_COUNT 24

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static size_t padding( const uint64_t width )
{
	return width % 4;
}

static void bmp_header_read( FILE* in, struct bmp_header* bmp_header )
{
	fread(bmp_header, sizeof(struct bmp_header), 1, in);
}

static enum read_status bmp_header_check( struct bmp_header bmp_header )
{
	if ( bmp_header.bfType != BF_TYPE )
	{
		return READ_INVALID_SIGNATURE;
	}
	else if ( bmp_header.biBitCount != BI_BIT_COUNT )
	{
		return READ_INVALID_BITS;
	}
	else if ( bmp_header.bfReserved != 0 || bmp_header.bOffBits != sizeof(struct bmp_header) )
	{
		return READ_INVALID_HEADER;
	}
		
	return READ_OK;
}

static struct pixel* img_row( const size_t row, const uint64_t width, struct pixel* data )
{
	return data + row * width;
}

enum read_status from_bmp( FILE* in, struct image* img ) 
{
	if (in == NULL || img == NULL)
	{
		return READ_ERROR;
	}
	else
	{
		struct bmp_header bmp_header = {0};
		
		bmp_header_read(in, &bmp_header);
		
		enum read_status header_read_status = bmp_header_check(bmp_header);
		
		if ( header_read_status != READ_OK)
		{
			return header_read_status;
		}
		else
		{
			*img = image_create(bmp_header.biWidth, bmp_header.biHeight);
		
			if ((*img).data == NULL)
			{
				return READ_ERROR;
			}
			
			uint8_t for_padding[4];
			
			for ( size_t i = 0; i < bmp_header.biHeight; i++ ) 
			{
				if ( fread(img_row(i, bmp_header.biWidth, (*img).data), sizeof(struct pixel) * bmp_header.biWidth, 1, in) != 1 || fread(for_padding, padding(bmp_header.biWidth), 1, in) != 1 )
				{
					return READ_ERROR;
				}
			}

			return READ_OK;
		}
	}
}

static size_t image_size( struct image img )
{
	return img.width * img.height * sizeof(struct pixel);
}

static size_t file_size( struct image img )
{
	return sizeof(struct bmp_header) + image_size(img) + padding( img.width ) * img.height;
}

static struct bmp_header bmp_header_from_img( struct image img )
{
	return (struct bmp_header) 
	{
		.bfType = BF_TYPE,
		.bfileSize = file_size(img),
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = BI_SIZE,
		.biWidth = img.width,
		.biHeight = img.height,
		.biPlanes = 1,
		.biBitCount = BI_BIT_COUNT,
		.biCompression = 0,
		.biSizeImage = image_size(img),
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0
	};
}

enum write_status to_bmp( FILE* out, struct image const* img ) 
{
	if ( out == NULL || img == NULL || (*img).data == NULL )
	{
		return WRITE_ERROR;
	}
	else
	{
		struct bmp_header bmp_header = bmp_header_from_img(*img);

		if ( fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1 )
		{
			return WRITE_ERROR;
		}
		
		uint8_t for_padding[4] = {0, 0, 0, 0};
		
		for ( size_t i = 0; i < (*img).height; i++ )
		{
			if ( fwrite(img_row(i, (*img).width, (*img).data), sizeof(struct pixel) * (*img).width, 1, out) != 1 || fwrite(for_padding, padding((*img).width), 1, out) != 1 )
			{
				return WRITE_ERROR;
			}
		}

		return WRITE_OK;
	}
}
