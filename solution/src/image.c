#include "image.h"

#include <stdlib.h>

struct image image_create( uint64_t width, uint64_t height )
{
	return (struct image) 
	{
		.width = width,
		.height = height,
		.data = malloc(width * height * sizeof(struct pixel))
	};
}

void image_free( struct image* img )
{
	if ( (*img).data != NULL )
	{
		free((*img).data);
	}
}
