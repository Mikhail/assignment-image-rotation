#include <stdio.h>
#include <stdlib.h>

#include <inttypes.h>

#include "bmp.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    
    struct image img = {0};
    
    FILE* src = fopen(argv[1], "rb");
    
    if ( src == NULL )
    {
    	fprintf(stderr, "Проблемы с открытием входного файла\n");
    	return 1;
    }
    
    if ( from_bmp(src, &img) != READ_OK )
    {
    	fprintf(stderr, "Проблемы с чтением файла\n");

		image_free(&img);

    	if ( fclose(src) == EOF )
    	{
    		fprintf(stderr, "Проблемы с закрытием входного файла\n");
    		
    		return 3;
    	}
    	
    	return 2;
    }
    
   	if ( fclose(src) == EOF )
    {
    	fprintf(stderr, "Проблемы с закрытием входного файла\n");
    		
    	return 4;
    }
    
    struct image img2 = rotate(img);
    
    image_free(&img);
    
    FILE* dst = fopen(argv[2], "wb");
    
    if ( dst == NULL )
    {
    	fprintf(stderr, "Проблемы с открытием выходного файла\n");
    	return 5;
    }
    
    if ( to_bmp(dst, &img2) != WRITE_OK )
    {
    	fprintf(stderr, "Проблемы с записью в файл\n");
    
    	image_free(&img2);
    
    	if ( fclose(dst) == EOF )
    	{
    		fprintf(stderr, "Проблемы с закрытием выходного файла\n");
    		return 7;
    	}
    	
    	return 6;
    }

	image_free(&img2);
    
    if ( fclose(dst) == EOF )
    {
    	fprintf(stderr, "Проблемы с закрытием выходного файла\n");
    	return 8;
    }

    return 0;
}
